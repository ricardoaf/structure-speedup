const allIdsDB = require('./ids-banco.json')
const { evalStructureSerial, evalStructureParallel } = require('./Services.js');


function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
  

// input
//-------------------------------------------------------------------------------------------------
const user = 'CQ8B';
const pass = 'CQ8B';
const loginURL = 'http://localhost:6002/login'
const requestURL = 'http://localhost:6002/structure/filter'
const allStructureID = allIdsDB.map(({id})=>id);


// serial test
//-------------------------------------------------------------------------------------------------
const serialTime = [];
// const samples = [100, 200, 500, 1000];
const samples = [1000];
const structureSet = N => allStructureID.slice(0, N);

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}





const serialLoop = async () =>{
    for (let i=0; i<samples.length; i++){
        const N = samples[i];
        const StructureID = structureSet(N);
        const { time } = await evalStructureSerial(user, pass, loginURL, requestURL, StructureID);
        console.log('N', N, 'time', time)
        serialTime.push({N, time: 1*time.toFixed(0)});
        await sleep(10000)
    }

    console.table(serialTime)
}


const parallelTime = [];
const cases = [
  // {N: 10, simultaneous: 2}, {N: 10, simultaneous: 5}, {N: 10, simultaneous: 10},
   //  {N: 100, simultaneous: 2}, {N: 100, simultaneous: 5}, {N: 100, simultaneous: 10},
 {N: 1000, simultaneous: 1},
 {N: 1000, simultaneous: 2},
 {N: 1000, simultaneous: 5},
 {N: 1000, simultaneous: 10},
]


const parallelLoop = async ()=>{
    for (let i=0; i<cases.length; i++){
        const batch = cases[i];
        const {N, simultaneous} = batch;
        const structure = structureSet(N);
        const time = await evalStructureParallel(user, pass, loginURL, requestURL, structure, simultaneous);

        parallelTime.push({N, simultaneous, time: 1*time.toFixed(0)});
        await sleep(10000)
    }

    console.table(parallelTime)

}


const run = async ()=>{
    await serialLoop();
    // await parallelLoop();
}

run();




/*
// parallel test
//-------------------------------------------------------------------------------------------------

*/