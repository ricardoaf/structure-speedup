const axios = require('axios');
const PromisePool = require('@supercharge/promise-pool')

const login = async (username, password, loginURL) => {
    try {
        const {data} = await axios.request({
            method: 'POST', url: loginURL,
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
            },
            data: { username, password },
        });
        return data
    } catch (e) { console.error(e); }
};


const requestStructureSet = async (token, requestURL, structureID) => {
    try {
        const startTime = new Date();
        const res = await axios.request({
            method: 'POST', url: requestURL,
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: `Bearer ${token}`,
            },
            data: {
                "onlyValidated": false,
                "metadata": false,
                "historic": false,
                "withMyPendingSuggestion": false,
                "externalProps": false,
                "parallel": true,
                "filter": {
                    "field": "id",
                    "data": structureID
                }
            },
        });
        const time = (new Date() - startTime);
        return { res, time };
    } catch (e) { console.error(e); }
};


const evalStructureSerial = async (username, password, loginURL, requestURL, structureID) => {
    const { token } = await login(username, password, loginURL);
    const { res, time } = await requestStructureSet(token, requestURL, structureID);
    return { res, time };
};


const evalStructureParallel
 = async (username, password, loginURL, requestURL, structureID, simultaneous) => {
    const { token } = await login(username, password, loginURL);



    const startTime = new Date();
    const structuresBatchs = [];


    for (let i=0;i<simultaneous;i++){
        structuresBatchs.push(structureID.slice(i*structureID.length/simultaneous, (i+1)*structureID.length/simultaneous))
    }


    const responses = await Promise.all(structuresBatchs.map(batch=>requestStructureSet(token, requestURL, batch)))


    const response =[];

    for (let j=0; j<responses.length;j++){
        const {res} = responses[j];
        response.push(...res.data)
    }

    const time = (new Date() - startTime);

    return time;


    /*const { res, time } = await PromisePool
        .for(structureID)
        .withConcurrency(simultaneous)
        .process(async data => {
            const { res, time } = requestStructureSet(token, requestURL, data);
            return { res, time };
        });*/
    //return { token, res, time };
};


module.exports = {
    evalStructureSerial,
    evalStructureParallel,
};
